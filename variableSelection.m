%% Function that selects variables dependent with the class variable
% based on an unconditional independence test

% Inputs:
%       X - (N x M) matrix; N is the number of samples and M is the number
%           of variables
%       Y - (N x 1) vector; the target class of each sample
%       dcX - (M x 1) vector; number of distinct values for each variable
%       dcY - (C x 1) vector; number of distinct classes
%       a - scalar; threshold for the independence test
% Outputs:
%       variables - vector; vector with the selected variables

function [variables] = variableSelection(X, Y, dcX, dcY, a)
    % get the number of variables
    M = size(X,2);

    % initialize a vector that will store the p-values for every variable
    pvalues = zeros(M,1);
    
    % for every variable
    for i=1:M
        % call independenceTest.m and get an estimated p-value
        % calling it with 0 at the end chooses X^2 test over G-test (1)
        [pvalue,~] = independenceTest(X(:,i), Y, dcX(i), dcY, 0);
        % store the p-value
        pvalues(i,1) = pvalue;
    end
    
    % find the statistically significant dependent variables (see line 42)
    % H0: every variable in X is independent of Y
    % thus, variables with p-values < a are dependent of Y and as such
    % informative of Y
    variables = sign_level(pvalues,a);
end

%% discover the variables that are not independent of the class variable
% given a significance level
function [sign_vars] = sign_level(pvalues,a)
    % find for which pairs the pvalue is smaller than the significance level
    sign_vars = find(pvalues<a);
end
