%% Function that selects the best model and parameters using a k-fold Cross
% Validation

% Inputs:
%       X - (N x M) matrix; N is the number of samples and M is the number
%           of variables
%       Y - (N x 1) vector; the target class of each sample
%       k - scalar; the number of desired folds to be created
% Outputs:
%       model - trained model using all the training (or provided) data and
%               the bestparameters
%       bestparameters - alpha, model description and indexes of variables
%                        used given a threshold

function [model, bestparameters] = modelSelection_CV(X, Y, dcX, dcY, k)
    % initialization of best parameters
    bestparameters = cell(3,1);

    % definition of the significance levels that will be used as thresholds
    alphas = [0.01, 0.05, 0.1, 1.0];
    
    % define the classifiers that will be used and the "code" that will be
    % returned in bestparameters
    % 11: Naive Bayes - Uniform Prior
    % 12: Naive Bayes - Empirical Prior
    % 21: Random Forest with 50 trees - MinLeafSize == 1
    % 22: Random Forest with 50 trees - MinLeafSize == 3
    % 23: Random Forest with 50 trees - MinLeafSize == 5
    % 31: KNN - K == 5
    % 32: KNN - K == 15
    % 33: KNN - K == 25
    % 41: SVM, KernelScale = auto, Standardize=on - KernelFunction = linear
    % 42: SVM, KernelScale = auto, Standardize=on - KernelFunction = rbf
    % 43: SVM, KernelScale = auto, Standardize=on - KernelFunction = polynomial
    classifiers = [11, 12, 21, 22, 23, 31, 32, 33, 41, 42, 43];
    
    % split data to k folds
    [folds] = splitDataToFolds(X, Y, k);
    
    % initialization of cell array that stores the different (k) training and
    % tune sets
    train_set = cell(k-1,k);
    tune_set = cell(1,k);
    
    % leave one fold out, as it should be the tune set; the rest will be
    % used as the training set (in case the algorithm is called not for
    % performance estimation, but rather for picking the best model, the
    % same logic applies; only this time we split in train and test)
    for i=1:k
        temp_folds = folds;
        tune_set{1,i} = temp_folds{i,1};
        temp_folds(i) = [];
        for j=1:k-1
            train_set{j,i} = temp_folds{j};
        end
    end
    
    % initialization of cell array that stores the indexes of the variables
    % that are selected given a threshold
    indexes = cell(1,length(alphas));
    
    % selection of the variables that are most informative of the classes
    for a=1:length(alphas)
       [variables] = variableSelection(X, Y, dcX, dcY, alphas(a));
       indexes{1,a} = variables;
    end
    
    % initialization of matrix that stores the accuracy of each configuration
    % on the training sets
    train_accuracies = zeros(length(classifiers),length(alphas));
    
    % train each configuration on each training set and get the accuracy by
    % testing the model on the respective tune set
    for a=1:length(alphas)                
        for c=1:length(classifiers)
            % initialize a vector for the accuracy on each fold
            acc = zeros(k,1);
            
            for f=1:k
                % convert the cells into matrices and retrieve the class labels
                train_XY = cell2mat(train_set(:,f));
                train_Y = train_XY(:,end);
                train_X = train_XY(:,1:end-1);
    
                tune_XY = cell2mat(tune_set(1,f));
                tune_Y = tune_XY(:,end);
                tune_X = tune_XY(:,1:end-1);
                
                % select to train and tune only on the corresponding
                % variables given a threshold
                train_X = train_X(:,indexes{1,a});
                tune_X = tune_X(:,indexes{1,a});
                
                % train on the training set of a specific fold and threshold
                % with each classifier (see line 137)
                Mdl = fit(train_X, train_Y, classifiers(c));
            
                % estimate the accuracy on this fold and save it
                acc(f,1) = evaluateModel(Mdl, tune_X, tune_Y);
            end
            
            % estimate the mean accuracy and save it
            accuracy = mean(acc);
            train_accuracies(c,a) = accuracy;   
        end
    end
    
    % choose the best accuracy and return the bestparameters
    [~,index] = max(train_accuracies(:));
    [row,col] = ind2sub(size(train_accuracies),index);
    bestparameters{1,1} = alphas(col);
    bestparameters{2,1} = classifiers(row);
    bestparameters{3,1} = indexes{1,col};
    
    % pick the configuration with the highest accuracy and train it on the
    % whole train-tune set; return the model
    model_X = X(:,indexes{1,col});
    model_Y = Y;
    model = fit(model_X, model_Y, classifiers(row));
    
end

%% Function that returns a model for each classifier's training
function [Mdl] = fit(X, Y, classifier)
    % use each of the predifined classifiers in order to produce a model
    % all the predictors are categorical
    if classifier==11
        % Naive Bayes - Uniform Prior
        Mdl = fitcnb(X,Y,'Prior','uniform','CategoricalPredictors','all');
    elseif classifier==12
        % Naive Bayes - Empirical Prior
        Mdl = fitcnb(X,Y,'Prior','empirical','CategoricalPredictors','all');
    elseif classifier==21
        % Random Forest with 50 trees - MinLeafSize == 1
        Mdl = TreeBagger(50,X,Y,'MinLeafSize',1,'CategoricalPredictors','all');
    elseif classifier==22
        % Random Forest with 50 trees - MinLeafSize == 3
        Mdl = TreeBagger(50,X,Y,'MinLeafSize',3,'CategoricalPredictors','all');
    elseif classifier==23
        % Random Forest with 50 trees - MinLeafSize == 5
        Mdl = TreeBagger(50,X,Y,'MinLeafSize',5,'CategoricalPredictors','all');
    elseif classifier==31
        % KNN - K == 5
        Mdl = fitcknn(X,Y,'NumNeighbors',5,'CategoricalPredictors','all');
    elseif classifier==32
        % KNN - K == 15
        Mdl = fitcknn(X,Y,'NumNeighbors',15,'CategoricalPredictors','all');
    elseif classifier==33
        % KNN - K == 25
        Mdl = fitcknn(X,Y,'NumNeighbors',25,'CategoricalPredictors','all');
    elseif classifier==41
        % SVM, KernelScale = auto, Standardize=on - KernelFunction = linear
        Mdl = fitcsvm(X,Y,'KernelFunction','linear','CategoricalPredictors','all',...
                        'KernelScale','auto','Standardize',true);
    elseif classifier==42
        % SVM, KernelScale = auto, Standardize=on - KernelFunction = rbf
        Mdl = fitcsvm(X,Y,'KernelFunction','rbf','CategoricalPredictors','all',...
                        'KernelScale','auto','Standardize',true);
    elseif classifier==43
        % SVM, KernelScale = auto, Standardize=on - KernelFunction =
        % polynomial
        Mdl = fitcsvm(X,Y,'KernelFunction','polynomial','CategoricalPredictors','all',...
                        'KernelScale','auto','Standardize',true);
    end
end
