%% X^2 and G-test of independece for discrete variables

% Input:
%        X -(N,1) vector of discrete values
%        Y - (N,1) vector of discrete values
%        dcX - number of possible values for variable X
%        dcY - number of possible values for variable Y
%        test - a number that indicates the statistic to be used (0 for
%               chi-squared test and 1 for G-test)
% Output:
%         pvalue - p-value returned by the test
%         statistic - statistic of the test

function [pvalue, statistic] = independenceTest(X, Y, dcX, dcY, test)

    % make sure that one of the two tests is choosen
    if ~ (test == 0 || test == 1)
        error = "'test' variable should take a value of 0 or 1";
        baseException = MException(error);
        throw(baseException);
    end
    
    % compute the degrees of freedom
    df = (dcX-1)*(dcY-1);
    
    % create the contigency matrix
    Obs = crosstab(X,Y);
    
    % find the total number of observations
    total_obs = sum(sum(Obs));
    
    % find total per row (corresponds to last col in a contingency table)
    % and column (corresponds to last row in a contingency table)
    last_col = sum(Obs,2);
    last_row = sum(Obs);
    
    % create an expected values matrix (this is equivalent to arrays
    % multiplication; the last column times the last row, divided by the
    % total number of observations)
    Exp = last_col*last_row./total_obs;
    
    % compute the statistic (see lines 62 and 67)
    if test==0
        statistic = chi_squared(Obs,Exp);
    elseif test==1
        statistic = G_test(Obs,Exp);
    end
    
    % compute the pvalue of the test (the chi-squared and the G-test have
    % the same probability density function)
    % the pvalue is the probability to observe a statistic at least as
    % extreme as in the distribution, or equivalently 1-cdf
    pvalue = 1-chi2cdf(statistic,df);
    
end

%% compute the X^2 statistic
function [statistic] = chi_squared(observed, expected)
    statistic = sum(sum( (observed - expected).^2./expected ));
end

%% compute the G-test
function [statistic] = G_test(observed, expected)
    lg = log(observed./expected);
    % because of the possibility of inf appearing after the log (since the
    % observed values could potentially equal 0), specifically handle this
    % case, replacing the inf by 0; the results highly follow the
    % chi_squared ones in this way
    lg(isinf(lg)) = 0;
    statistic = 2*sum(sum( observed.*lg ));
end
