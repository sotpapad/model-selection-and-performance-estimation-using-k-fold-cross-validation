%% load the data
load('data.mat');

%% main algorithm - repeat the model selection and performance estimation

% repetitions of algorithm
reps = 25;

% folds to be used for spliting the data
k = 5;

% different percentages of train-test splits to be explored (defined as
% percentage of training sample)
percentages = [9/10, 8/10, 7/10];

% initialization of multi-dimensional cell array that stores the results
% rows: repetitions
% columns: percentage of train-test sets
% first page: estimated accuracy for each repetition and percentage
% second page: bestparameters (as defined in modelSelection_CV.m)
results = cell(reps,length(percentages),2);

% model selection on all data
[model, bestparameters] = modelSelection_CV(X, Y, dcX, dcY, k);

for r=1:reps
   for p=1:length(percentages)
       % merge input variables and class vector
       XY = [X Y];
       
       % split in training and test sets (see line 111)
       [train_set, test_set] = random_split(XY,percentages(p));
       
       % split each set in X and Y
       train_X = train_set(:,1:end-1);
       train_Y = train_set(:,end);
       test_X = test_set(:,1:end-1);
       test_Y = test_set(:,end);
       
       % recalculation of distinct classes of X and Y for training set
       % (see line 99)
       [train_X,train_dcX] = process_values(train_X);
       [train_Y,train_dcY] = process_values(train_Y);
       [test_X,~] = process_values(test_X);
       [test_Y,~] = process_values(test_Y);
       
       % performance estimation
       % 1. cross-validation on training data
       [train_model,train_bestparameters] = modelSelection_CV(train_X,...
                                            train_Y, train_dcX, train_dcY, k);
       results{r,p,2} = train_bestparameters;
       % 2. accuracy estimation on test set
       [accuracy] = evaluateModel(train_model, test_X(:,train_bestparameters{3,1}),...
                    test_Y);
       results{r,p,1} = accuracy;
   end
end

%% save all the variables of the workspace
save("results.mat",'results')

%% create plot to visualize the results

% specify bar locations in x axis (according to percenatge of test set)
x = [10 20 30];

% extract the info from the results
split10 = cell2mat(results(:,1,1));
split20 = cell2mat(results(:,2,1));
split30 = cell2mat(results(:,3,1));

% compute the mean values and the std
m10 = mean(split10);
var10 = var(split10);
m20 = mean(split20);
var20 = var(split20);
m30 = mean(split30);
var30 = var(split30);

% visualization of the mean accuracy of all algorithm repetitions for each
% split of the data in training and test sets
figure(1);
bar(x,[m10 m20 m30],'FaceColor',[0.2 0.2 1]); yticks(0.5:0.05:1); ylim([0.5,1]);
grid on;
xlabel('Percenatge of samples in test set (%)');
title('Mean accuracy per test set size');

figure(2);
bar(x,[var10 var20 var30],'FaceColor',[0.8 0.2 0.2]);
grid on;
xlabel('Percenatge of samples in test set (%)');
title('Variance per test set size');

%% make sure that the valriables and class have minimum possible value of 0,
% then get their possible values
function [A,dcA] = process_values(A)
    % find the minimum value corresponding to a variable
    min_cat_A = min(A);
    % make the values start from 0 (their number remains the same; what
    % changes is their labels) if their already do not
    A = A-min_cat_A;
    % get the number of possible values
    dcA = max(A)+1;
end

%% split the dataset in train-test partitions
function [train_set,test_set] = random_split(A,percentage)
    % 70% can not always be integer, so use round
    J = size(A,1);
    split = round(percentage*J);

    % randomly pick split% of the samples, make them the training set and use
    % the rest as the test set
    [train_set, ids_train] = datasample(A,split,1,'Replace',false);
    ids_test = setdiff(1:J,ids_train);
    test_set = A(ids_test,:);
end
