Model Selection and Performance Estimation of Naive Bayes, SVM, k-NN and Random
Forests classifiers (with different configurations) on categorical, data using
the k-fold cross-validation algorithm. A (naive) variable selection is implemented
with the X^2 test of independence.

Main.m is responsible for making the calls to the other functions. The process of
performance estimation is repetead so that a better estimation may be acquired.